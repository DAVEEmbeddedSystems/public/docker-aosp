Android Open Source Project Docker Build Environment
====================================================

This is originally taken from [this GitHub project](https://github.com/kylemanna/docker-aosp.git)


Quickstart
----------

Prepare aosp source code

```bash
mkdir sources && cd sources
CLONE-YOUR-AOSP-HERE
```

Build docker image (if needed)

```bash
docker build -t dave/aosp:m6 http://gitlab.lan.dave.eu/mtsx/docker-aosp.git
curl -O http://gitlab.lan.dave.eu/mtsx/docker-aosp/raw/mtsx/utils/aosp
chmod +x aosp

mkdir /opt/aosp/sources && CLONE-YOUR-AOSP-HERE
```

Every time you need to build aosp, setup the environment

```bash
export AOSP_IMAGE=dave/aosp:m6
export AOSP_VOL=/opt/aosp/sources
./aosp
```

This will run the container and open a shell where you can run usual aosp commands

```bash
source build/envsetup.sh
lunch WHATHEVER
make -j VERYBIGNUMBER
```
